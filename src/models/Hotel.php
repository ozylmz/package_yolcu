<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function country(){
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function reviews()
    {
        return $this->morphMany('App\Review', 'reviewable');
    }
}
