<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function hotels(){
        return $this->hasMany('App\Hotel', 'country_id');
    }

    public function reviews()
    {
        return $this->morphMany('App\Review', 'reviewable');
    }
}
