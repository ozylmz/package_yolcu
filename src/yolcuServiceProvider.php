<?php

namespace yolcu\yolcu;

use Illuminate\Support\ServiceProvider;

class yolcuServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/migrations' => database_path('migrations'),
        ],'migrations');

        $this->publishes([
            __DIR__.'/models' => app_path(),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}